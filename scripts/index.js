function askAge(previousAge) {
  let age = prompt("Будь ласка, введіть ваш вік:", previousAge);
  if (age === null || age === "") {
    return askAge(previousAge);
  }
  if (isNaN(age)) {
    return askAge(previousAge);
  }
  return age;
}
function askName(previousName) {
  let name = prompt("Будь ласка, введіть ваше ім'я:", previousName);
  if (name === null || name === "") {
    return askName(previousName);
  }
  return name;
}
let userName = askName("");
let userAge = askAge("");
if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
  let confirmResult = confirm("Are you sure you want to continue?");
  if (confirmResult) {
    alert(`Welcome, ${userName}!`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome, ${userName}!`);
}
